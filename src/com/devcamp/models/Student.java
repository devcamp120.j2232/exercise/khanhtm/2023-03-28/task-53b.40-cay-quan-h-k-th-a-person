package com.devcamp.models;

public class Student extends Person {
 /**
  * Thuộc tính khởi tạo
  */
 private String program;
 private int year;
 private double fee;
 /**
  * Phương thức khởi tạo
  */
 public Student(String name, String address, String program, int year, double fee) { 
  super(name, address);
  this.program = program;
  this.year = year;
  this.fee = fee;
 }
 /**
  * getter method
  * @return
  */
  public String getProgram() {
   return program;
  }

  public int getYear() {
   return year;
  }

  public double getFee() {
   return fee;
  }
  /**
   * Setter method
   * @param
   */
  public void setProgram(String program) {
   this.program = program;
  }

  public void setYear(int year) {
   this.year = year;
  }

  public void setFee(double fee) {
   this.fee = fee;
  }

  @Override
  public String toString() {
   return "Student[Person[name=" + this.getName() + ", address=" + this.getAddress() + "], program=" + this.program + ", year=" + this.year + ", fee=" + this.fee + "]";
  }
}
