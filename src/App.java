import com.devcamp.models.Person;
import com.devcamp.models.Staff;
import com.devcamp.models.Student;

public class App {
    public static void main(String[] args) throws Exception {
       Person person1 = new Person("Khanh Tran", "HCM");
        System.out.println("person1:");
        System.out.println(person1);

       Person person2 = new Person("Khánh Sơn", "Ngọc Hồi");
       System.out.println("person2:");
        System.out.println(person2);

        Student student1 = new Student("Hoàng Hà", "Mỹ Tho","JAVA DEV",1990,20000.00d);
        System.out.println("student1:");
        System.out.println(student1);

        Student studen2 = new Student("Ngọc Ngọc", "Bình Tân","REACT DEV",1995,30000.00d);
        System.out.println("studen2:");
        System.out.println(studen2);

        Staff staff1 = new Staff("Trung Sơn", "Kiên Giang","Iron Hack",400000);
        System.out.println("staff1:");
        System.out.println(staff1);

        Staff staff2 = new Staff("Quỳnh Như", "Bình Thuận","Iron Hack",400000);
        System.out.println("staff2:");
        System.out.println(staff2);

    }
}
